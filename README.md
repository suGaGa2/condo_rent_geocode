# condominium / rent data

# How to use:

1. Import csv file to Target folder. When the file was converted once in the past, the file is moved to Duplicated Folder without any processing.
2. run `python COLLECT.py`  (Assuming python3 environment)
3. You can get the file in the Output Folder with names "JP_(original_file_name).csv" and "EN_(original_file_name).csv"




# APIs:
1. Google geocoding API
2. docomo API



# Problems
・建物名に部屋番号が付加されているとき，部屋番号情報のロス，建物名がおかしくなる．
・正常に変換されなかった部分を２回目変換するコード準備．そのためにエラーコード整備.
・Outputのファイル，県でまとめた方がいいかも？
